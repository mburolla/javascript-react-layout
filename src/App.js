import './App.scss';

function App() {

  return (
    <div className="App">

      <div>
        Non-responsive columns...
      </div>

      <div className="container-fluid">
        <div className="row">
          {
          /* 
            Bootstrap will never stack these divs because no responsive breakpoints have been specified.
            Since no column units have been specified, bootstrap equally divides these columns.
          */
          }
          <div className="col">1</div> 
          <div className="col">2</div>
          <div className="col">3</div>
        </div>

        <div className="row">
          {
          /* 
            Bootstrap will never stack these divs because no responsive breakpoints have been specified.
            The column units have been specified, therefore bootstrap renders the width of these columns.
          */
          }
          <div className="col-1">4</div> 
          <div className="col-1">5</div>
          <div className="col-10">6</div>
        </div>
      </div>

      <div>
        Responsive columns...
      </div>

      <div className="container-fluid">
        { /* A row has 12 columns */ }
        <div className="row">
          {
          /* 
            Bootstrap stacks these divs vertically, until the "sm" responsive breakpoint is hit.  
            When the "sm" breakpoint is hit, bootstrap divides the divs equally...
            each div is 4 units wide (4 * 3 = 12). We could implicity specify the unit size 
            for each div: col-sm-4.
          */
          }
          <div className="col-sm">7</div> 
          <div className="col-sm">8</div>
          <div className="col-sm">9</div>
        </div>
          {
          /* 
            Bootstrap stacks these divs vertically, until the "sm" responsive breakpoint is hit.  
            When the "sm" breakpoint is hit, bootstrap divides the divs equally...
            each div is 6 units wide (6 * 2 = 12). We could implicity specify the unit size 
            for each div: col-sm-6.
          */
          }
        <div className="row">
          <div className="col-sm">10</div>
          <div className="col-sm">11</div>
        </div>
      </div>

      <div className="container-fluid">
        { /* A row has 12 columns */ }
        <div className="row gx-2 gy-2">  {/* gutter=padding x & y axis visible in "mobile" mode*/ }
          {
          /* 
          Bootstrap stacks these divs vertically until the "md" responsive breakpoint is hit.
          When the "md" breakpoint is hit, bootstrap assigns 4 column units to the
          sidebar div and 8 column units to the main content div.
          */
          }
          <div className="col-md-4">Sidebar</div>
          <div className="col-md-8">Main content here</div>
        </div>
      </div>

      <div className="container-fluid">
        { /* A row has 12 columns */ }
        <div className="row gx-2 gy-2">  {/* gutter=padding x & y axis visible in "mobile" mode*/ }
          {
          /* 
            We can add multiple responsive classes to our divs.
          */
          }
          <div className="col-md-4 col-lg-6">Sidebar</div>
          <div className="col-md-8 col-lg-6">Main content here</div>
        </div>
      </div>

      <div>
        Example Website...
      </div>
      
      <div className="container-fluid">
        { /* A row has 12 columns */ }
        <div className="row"> 
          {
          /* 
          Bootstrap stacks the items in the nav bar until the "lg" breakbout is hit.
          */
          }
          <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="container-fluid">
              <a className="navbar-brand" href="https://www.google.com">Navbar</a>
              <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
              </button>
              <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div className="navbar-nav">
                  <a className="nav-link active" aria-current="page" href="https://www.google.com">Home</a>
                  <a className="nav-link" href="https://www.google.com">Features</a>
                  <a className="nav-link" href="https://www.google.com">Pricing</a>
                  <a className="nav-link disabled" href="https://www.google.com">Disabled</a>
                  <form className="d-flex">
                    <input className="form-control me-2" type="search" placeholder="Search" aria-label="Search" />
                    <button className="btn btn-outline-success" type="submit">Search</button>
                  </form>
                </div>
              </div>
            </div>
          </nav>
        </div>
        <div className="row"> 
          <div className="col-md">
            <img className="img-fluid" src={require('./assets/gtr-1.jpg')} alt=""/>
          </div>
        </div>
        <div className="row"> 
           <div className="col-md-8">This is some text that will go here.This is some text that will go here.This is some text that will go here.This is some text that will go here.This is some text that will go here.This is some text that will go here.This is some text that will go here.This is some text that will go here.</div>
           <div className="col-md-4">
              <img className="img-fluid" src={require('./assets/gtr-1.jpg')} alt=""/> { /* Images expand to the size of their parent element*/}
           </div>
        </div>
      </div>

      <div>
        Flexbox...
      </div>

      { 
      /* p=padding px=padding x axis py=padding y axis */ 
      /* Padding goes from 1 to 5. */
      }
      <div className="d-flex flex-row my-content">
          <div className="my-sub-content-1">
            test
          </div>
          <div className="my-sub-content-2">
            test
          </div>
          <div className="my-sub-content-3">
            <div className="my-sub-content-4">
              cat
              dog
              cow
              <br />
              owl
              ant
              bee
            </div>
          </div>
          <div className="my-sub-content-5">
            This is some sample text.  This is some sample text. This is some sample text. This is some sample text.
            <br/>
            <button>Push Me</button>
          </div>
      </div>

      <div className="d-flex justify-content-between">
        <div className="p-2">Flex item 1</div>
        <div className="p-2">Flex item 2</div>
        <div className="p-2">Flex item 3</div>
        <div className="p-2">Flex item 4</div>
        <div className="p-2">Flex item 5</div>
      </div>

      <div className="d-flex justify-content-between">
        <div><button>LEFT</button></div>
        <div><button>RIGHT</button></div>
      </div>

    </div>
  );
}

export default App;
