# JavaScript React & Bootstrap
Reference repo that illustrates the Bootstrap grid system and Sassy CSS (SASS) for a React application.  The Bootstrap grid and flex systems will handle 99% of your layout requirements.  Sass will handle all of your stylistic requirements.

# Getting Started
- Clone this repo
- Install dependencies: `npm install`
- Run the app: `npm start`

# Videos
- [HTML and CSS Tutorial (2h)](https://youtu.be/D-h8L5hgW-w)
- [Bootstrap 5 Grid System Tutorial (15m)](https://youtu.be/DZKf9l42WCo)
- [Getting Started with Bootstrap 5 (1h)](https://youtu.be/1nxSE0R27Gg)

# Links
- [Bootstrap flex](https://getbootstrap.com/docs/4.0/utilities/flex/)
- [Bootstrap Utilities](https://getbootstrap.com/docs/4.0/utilities/borders/)
- [Common CSS Properties](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Properties_Reference)
- [Flex](https://getbootstrap.com/docs/5.1/utilities/flex/)
- [Common CSS Properties](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Properties_Reference)
- [SASS](https://youtu.be/Zz6eOVaaelI)

# Notes
A row in a bootstrap container has 12 columns.  If a unit size is not specified, bootstrap will divide the divs equally for the columns in the row.

# Responsive Breakpoints
A responsive breakpoint is "hit" when the browser is expanded to the width specified in this table:

|Breakpoint       |Class infix  |Width      |
|-----------------|-------------|-----------|
|X-Small          |None         |0-575px    |
|Small            |sm           |>=576px    |
|Medium           |md           |>=768px    |
|Large            |lg           |>=992px    |
|Extra Large      |xl           |>=1200px   |
|Extra extra large|xxl          |>=1400px   |

To view the width of the browser ensure that the Dev Tools console is open (ctl+shift+i) and observe the dimensions in the upper right hand corner of the browser window.

# Config
- Enabling bootstrap for a React application requires adding the following to `./public/index.html` in the head section:

```
 <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
```
